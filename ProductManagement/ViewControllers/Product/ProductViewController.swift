//
//  ProductViewController.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import UIKit
import Moya

class ProductViewController: BaseViewController {
    @IBOutlet weak var productTableView: UITableView!
    @IBOutlet weak var pageCollectionView: UICollectionView!
    @IBOutlet weak var editTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet var listView: UIView!
    
    var productViewModel: ProductViewModel = ProductViewModelFactory().makeViewModel()
    var colorViewModel: ColorViewModel = ColorViewModelFactory().makeViewModel()
    var products = [LocalProduct] ()
    var listEdit = [LocalProduct] ()
    var page = 0
    var numberOfPage = 0
    var transparentView = UIView()
    
    override func viewDidLoad() {
        GUI()
        super.viewDidLoad()
        productViewModel.delegate = self
        colorViewModel.delegate = self
        colorViewModel.readedColors()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        LoadItem(page: page)
    }
        
    func GUI(){
        productTableView.delegate = self
        productTableView.dataSource = self
        pageCollectionView.delegate = self
        pageCollectionView.dataSource = self
        editTableView.delegate = self
        editTableView.dataSource = self
        transparentView.backgroundColor = .black.withAlphaComponent(0.9)
        listView.layer.cornerRadius = 30
    }
    
    func LoadItem(page: Int){
        print(">>> Load page \(page)")
        products = productViewModel.getProductPage(at: page)
        self.productTableView.reloadData()
    }
    
    func addToList(newProduct: LocalProduct) {
        if let index = listEdit.firstIndex(where: {$0.id == newProduct.id}) {
            listEdit[index] = newProduct
        } else {
            listEdit.append(newProduct)
        }
        print(listEdit)
    }
    
    @IBAction func showEditList(_ sender: Any) {
        dialogIn ()
        LoadItem(page: page)
    }
    
    func dialogIn () {
        let backgroundView = self.view!
        transparentView.frame = self.view.frame
        transparentView.alpha = 0
        listView.frame = CGRect(x: 20, y: (self.view.frame.height - 500)/2, width: self.view.frame.width-40, height: 500)
        backgroundView.addSubview(transparentView)
        backgroundView.addSubview(listView)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickTransparentView))
        transparentView.addGestureRecognizer(tapGesture)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.listView.frame = CGRect(x: 20, y: (self.view.frame.height - 500)/2, width: self.view.frame.width-40, height: 500)
        }, completion: nil)
        editTableView.reloadData()
    }
    
    @objc func onClickTransparentView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.removeFromSuperview()
            self.listView.removeFromSuperview()
        }, completion: nil)
    }
    
    @IBAction func UpdateDataToLocal(_ sender: Any) {
        productViewModel.updateProductLocal(listProduct: listEdit)
        onClickTransparentView()
    }
    
}

extension ProductViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == productTableView) {
            return products.count
        }
        else {
            return listEdit.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell", for: indexPath) as! ProductTableViewCell
        if(tableView == productTableView) {
            let product = productViewModel.getProductView(product: products[indexPath.row], colors: colorViewModel.getAllColor())
            cell.generateCell(product: product)
        }
        else {
            let product = productViewModel.getProductView(product: listEdit[indexPath.row], colors: colorViewModel.getAllColor())
            cell.generateCell(product: product)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(tableView == productTableView) {
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProductDetail2ViewController") as! ProductDetail2ViewController
            vc.product = products[indexPath.row]
            vc.colors = colorViewModel.getAllColor()
            vc.mainViewController = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension ProductViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 40, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfPage
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        let cellWidth = 40
        let CellCount = numberOfPage
        let CellSpacing = 10
        let totalCellWidth = cellWidth * CellCount
        let totalSpacingWidth = CellSpacing * (CellCount - 1)

        let leftInset = (self.pageCollectionView.bounds.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset

        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PageCollectionViewCell", for: indexPath) as! PageCollectionViewCell
        cell.numberOfPage.text = String(indexPath.row+1)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.layer.borderWidth = 1.0
            cell?.layer.borderColor = AppColor.mainColor.cgColor
            page = indexPath.row
            LoadItem(page: page)
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderWidth = 0
    }
    
}

// MARK: ProductViewModelDelegate

extension ProductViewController: ProductViewModelDelegate, ColorViewModelDelegate {
    func showLoading() {
        showLoadingIndicator()
    }

    func hideLoading() {
        hideLoadingIndicator()
    }

    func showErrorMessage(message: String) {
        showErrorAlert(message: message)
    }

    func readedProducts() {
        numberOfPage = productViewModel.numberOfRowsInSection()%10 != 0 ? productViewModel.numberOfRowsInSection()/10 + 1 : productViewModel.numberOfRowsInSection()/10
        LoadItem(page: 0)
        self.pageCollectionView.reloadData()
    }
    
    func readedColors() {
        productViewModel.readedProducts()
    }
}
