//
//  ProductDetailViewController.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import UIKit
import Kingfisher
import DropDown
import SceneKit

class ProductDetail2ViewController: UIViewController {

    @IBOutlet weak var sceneView: SCNView!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productName: UITextField!
    @IBOutlet weak var productId: UILabel!
    @IBOutlet weak var productError: UILabel!
    @IBOutlet weak var productSKU: UITextField!
    @IBOutlet weak var colorSelect: UIButton!
    @IBOutlet var alertView: UIView!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    
    var mainViewController: ProductViewController?
    var product: LocalProduct?
    var colors = [Color]()
    let dropDown = DropDown()
    var transparentView = UIView()
    
    func loadingImg(imageView: UIImageView, url: String) {
        let url = URL(string: url)
        
        imageView.kf.setImage(with: url, placeholder: UIImage(named: "imgPlaceholder"), options: [
            .loadDiskFileSynchronously,
            .cacheOriginalImage,
            .transition(.fade(0.25))
        ])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GUI()
        SetData()
    }
    

    func GUI() {
        productImg.layer.cornerRadius = 100
        transparentView.backgroundColor = .black.withAlphaComponent(0.9)
        alertView.layer.cornerRadius = 30
        var list = [String]()
        for i in colors {
            list.append(i.name)
        }
        dropDown.dataSource = list
        backButton.isHidden = true
        sceneView.isHidden = true
    }
    
    func SetData() {
        loadingImg(imageView: productImg, url: product!.image)
        productName.text = product!.name
        productId.text = String(product!.id)
        productError.text = product!.errorDescription
        productSKU.text = product!.sku
        if (product!.color != nil) {
            for i in colors {
                if (i.id == product!.color!) {
                    colorSelect.setTitle(i.name, for: .normal)
                }
            }
          
        }
        sceneView.scene = SCNScene(named: "../Laptop/Laptop.obj")
    }
    
    @IBAction func changeColor(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.frame.size.height)
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let _ = self else { return }
            sender.setTitle(item, for: .normal)
        }
    }
    
    @IBAction func SaveChange(_ sender: Any) {
        if (validate()) {
            if checkUpdate() {
                let newProduct = LocalProduct()
                newProduct.id = product!.id
                newProduct.name = productName.text ?? ""
                newProduct.errorDescription = productError.text ?? ""
                newProduct.sku = productSKU.text ?? ""
                newProduct.image = product!.image
                newProduct.color = colors.first(where: {$0.name == colorSelect.titleLabel?.text})?.id
                mainViewController!.productViewModel.updateProduct(newProduct: newProduct)
                mainViewController!.addToList(newProduct: newProduct)
            }
            navigationController?.popViewController(animated: true)
        }
    }
    
    func checkUpdate() -> Bool {
        if (productName.text != product!.name || productSKU.text != product!.sku || colors.first(where: {$0.name == colorSelect.titleLabel?.text})?.id != product!.color) {
            return true
        }
        return false
    }
    
    func dialogIn (error: String) {
        errorMessage.text = error
        let backgroundView = self.view!
        transparentView.frame = self.view.frame
        transparentView.alpha = 0
        alertView.frame = CGRect(x: 40, y: (self.view.frame.height - 200)/2, width: self.view.frame.width-80, height: 200)
        backgroundView.addSubview(transparentView)
        backgroundView.addSubview(alertView)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickTransparentView))
        transparentView.addGestureRecognizer(tapGesture)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.alertView.frame = CGRect(x: 40, y: (self.view.frame.height - 200)/2, width: self.view.frame.width-80, height: 200)
        }, completion: nil)
    }
    
    func validate() -> Bool {
        let name = productName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let sku = productSKU.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        if name.isEmpty || sku.isEmpty {
            dialogIn(error: "Please input the missing fields!")
            return false
        } else {
            if name.count > 50 {
                dialogIn(error: "Product Name max length is 50 characters!")
                return false
            }
            if sku.count > 20 {
                dialogIn(error: "Product SKU max length is 20 characters!")
                return false
            }
        }
        
        return true
    }
    
    @IBAction func closeAlertView(_ sender: Any) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.removeFromSuperview()
            self.alertView.removeFromSuperview()
        }, completion: nil)
    }
    
    @objc func onClickTransparentView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.removeFromSuperview()
            self.alertView.removeFromSuperview()
        }, completion: nil)
    }
    
    @IBAction func openArView(_ sender: Any) {
        let vc = ARViewController()
        vc.product = product
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backToPreView(_ sender: Any) {
        sceneView.isHidden = true
        productImg.isHidden = false
        nextButton.isHidden = false
        backButton.isHidden = true
    }
    
    @IBAction func goToNextView(_ sender: Any) {
        sceneView.isHidden = false
        productImg.isHidden = true
        nextButton.isHidden = true
        backButton.isHidden = false
    }
}
