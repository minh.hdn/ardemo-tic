//
//  ARPickView.swift
//  ProductManagement
//
//  Created by SMin on 08/08/2023.
//

import Foundation
import UIKit
import ARKit
import RealityKit
import SceneKit

class ARViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {
    
    var product: LocalProduct?
    let configuration = ARWorldTrackingConfiguration()
    var shouldAddObject = true

    var arSCNView: ARSCNView = {
        let arSCNView = ARSCNView()
        arSCNView.translatesAutoresizingMaskIntoConstraints = false
        return arSCNView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        arSCNView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        arSCNView.autoenablesDefaultLighting = true
        arSCNView.delegate = self
        arSCNView.showsStatistics = true
//        arSCNView.enableObjectScaling()
        arSCNView.enableObjectRotate()
        arSCNView.enableObjectMove()
    }

    func setupView() {
        view.addSubview(arSCNView)
    
        arSCNView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        arSCNView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        arSCNView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        arSCNView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
      }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            
        // Create a session configuration
        configuration.planeDetection = .horizontal
        UIApplication.shared.isIdleTimerDisabled = true
        self.arSCNView.autoenablesDefaultLighting = true
        // Run the view's session
        arSCNView.session.run(configuration)
        addGestures()
        arSCNView.session.delegate = self
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        arSCNView.session.pause()
    }
    
//    func pickOnPlane(anchor: ARPlaneAnchor) -> SCNNode {
//
//        let objectScene = SCNScene(named: "../Laptop/Laptop.obj")
//        guard let objectNode = objectScene?.rootNode.childNodes.first else {
//            print(">>>>> Model is not found!")
//            return SCNNode()
//        }
//        objectNode.scale = SCNVector3(anchor.extent.x/2, anchor.extent.x/2, anchor.extent.x/2)
//        objectNode.name = "object"
//        objectNode.position = SCNVector3(anchor.center.x, anchor.center.y, anchor.center.z)
//        return objectNode
//    }
    
    func removeNode(name: String) {
        arSCNView.scene.rootNode.enumerateChildNodes { (node, _) in
            if node.name == name {
                node.removeFromParentNode()
            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let anchorPlane = anchor as? ARPlaneAnchor else {
            return
        }
        print(">>>>> New plane anchor found at extent:", anchorPlane.extent)
//        let objectNode = pickOnPlane(anchor: anchorPlane)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let anchorPlane = anchor as? ARPlaneAnchor else {
            return
        }
        print(">>>>> Plane anchor update at extent:", anchorPlane.extent)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        guard let anchorPlane = anchor as? ARPlaneAnchor else {
            return
        }
        print(">>>>> Plane anchor remove at extent:", anchorPlane.extent)
    }
    
    func addGestures () {
        let tapped = UITapGestureRecognizer(target: self, action: #selector(tapGesture))
        arSCNView.addGestureRecognizer(tapped)
    }
       
    @objc func tapGesture (sender: UITapGestureRecognizer) {
        arSCNView.scene.rootNode.enumerateChildNodes { (child, _) in
            if child.name == "Object" {
                child.opacity = 1
                shouldAddObject = false
            }
        }
    }
    
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        let location = arSCNView.center
        let hitTest = arSCNView.hitTest(location, types: .existingPlaneUsingExtent)
            
        if hitTest.isEmpty {
            print("No Plane Detected")
            return
        } else {
                
            let columns = hitTest.first?.worldTransform.columns.3
                    
            let position = SCNVector3(x: columns!.x, y: columns!.y, z: columns!.z)
                    
            var node = arSCNView.scene.rootNode.childNode(withName: "Object", recursively: false) ?? nil
            if shouldAddObject == true {
                if node == nil {
                    let objectScene = SCNScene(named: "../Laptop/Laptop.obj")
                    node = objectScene?.rootNode.childNodes.first
                    node!.opacity = 0.7
                    let min = node!.boundingBox.min
                    let max = node!.boundingBox.max
                    let width = CGFloat(max.x - min.x)
                    let scale =  Float((product?.width ?? width) / width)
                    node!.scale = SCNVector3(x:  scale, y: scale, z: scale)
                    let columns = hitTest.first?.worldTransform.columns.3
                    node!.name = "Object"
                    node!.position = SCNVector3(x: columns!.x, y: columns!.y, z: columns!.z)
                    arSCNView.scene.rootNode.addChildNode(node!)
                } else {
                    let position2 = node?.position
                    if position == position2! {
                        return
                    } else {
                        let action = SCNAction.move(to: position, duration: 0.1)
                        node?.runAction(action)
                    }
                }
            }
        }
    }
}

func == (left: SCNVector3, right:SCNVector3) -> Bool {
    if (left.x == right.x && left.y == right.y && left.z == right.z) {
        return true
    } else {
        return false
    }
}

extension ARSCNView {
    
//    func enableObjectScaling() {
//        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(scaleObject(gesture:)))
//        self.addGestureRecognizer(pinchGestureRecognizer)
//    }
//
//    @objc func scaleObject(gesture: UIPinchGestureRecognizer) {
//
//        let location = gesture.location(in: self)
//        let hitTestResults = self.hitTest(location)
//        guard let nodeToScale = hitTestResults.first?.node else {
//            return
//        }
//
//        if gesture.state == .changed {
//
//            let pinchScaleX: CGFloat = gesture.scale * CGFloat((nodeToScale.scale.x))
//            let pinchScaleY: CGFloat = gesture.scale * CGFloat((nodeToScale.scale.y))
//            let pinchScaleZ: CGFloat = gesture.scale * CGFloat((nodeToScale.scale.z))
//            nodeToScale.scale = SCNVector3Make(Float(pinchScaleX), Float(pinchScaleY), Float(pinchScaleZ))
//            gesture.scale = 1
//
//        }
//        if gesture.state == .ended { }
//    }
    
    func enableObjectRotate() {
        let rotateGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(rotateObject(gesture:)))
        self.addGestureRecognizer(rotateGestureRecognizer)
    }
    
    @objc func rotateObject(gesture: UIRotationGestureRecognizer) {
        
        let location = gesture.location(in: self)
        let hitTestResults = self.hitTest(location)
        guard let node = hitTestResults.first?.node else {
            return
        }

        //Store The Rotation Of The CurrentNode
        var currentAngleY: Float = 0.0

        //1. Get The Current Rotation From The Gesture
        let rotation = Float(gesture.rotation)

        //2. If The Gesture State Has Changed Set The Nodes EulerAngles.y
        if gesture.state == .changed{
            node.eulerAngles.y = currentAngleY + rotation
        }

        //3. If The Gesture Has Ended Store The Last Angle Of The Cube
        if(gesture.state == .ended) {
            currentAngleY = node.eulerAngles.y
        }
    }
    
    func enableObjectMove() {
        let moveGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(moveObject(gesture:)))
        self.addGestureRecognizer(moveGestureRecognizer)
    }
    
    @objc func moveObject(gesture: UIPanGestureRecognizer) {

        let location = gesture.location(in: self)
        let hitTestResults = self.hitTest(location)
        guard let node = hitTestResults.first?.node else {
            return
        }
            
        guard let query = self.raycastQuery(from: location, allowing: .existingPlaneInfinite, alignment: .any) else { return }
        let results = self.session.raycast(query)
        guard let hitTestResult = results.first else {
            print("No surface found")
            return
        }

        //3. Convert To World Coordinates
        let worldTransform = hitTestResult.worldTransform

        //4. Set The New Position
        let newPosition = SCNVector3(worldTransform.columns.3.x, worldTransform.columns.3.y, worldTransform.columns.3.z)

        //5. Apply To The Node
        node.position = newPosition
    }
}

extension Double {
    func deg2rad() -> Double {
        return self * .pi / 180
    }
}
