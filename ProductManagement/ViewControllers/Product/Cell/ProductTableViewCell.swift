//
//  ProductTableViewCell.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import UIKit
import Kingfisher

class ProductTableViewCell: UITableViewCell {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productError: UILabel!
    @IBOutlet weak var productSKU: UILabel!
    @IBOutlet weak var ProductColor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        img.layer.cornerRadius = 20
        view.layer.cornerRadius = 10
        view.layer.shadowRadius = 3
        view.layer.shadowColor = AppColor.shadowColor.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 2, height: 1)
    }
    
    func loadingImg(imageView: UIImageView, url: String) {
        let url = URL(string: url)
        
        imageView.kf.setImage(with: url, placeholder: UIImage(named: "imgPlaceholder"), options: [
            .loadDiskFileSynchronously,
            .cacheOriginalImage,
            .transition(.fade(0.25))
        ])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func generateCell(product: ProductView) {
        loadingImg(imageView: img, url: product.image)
        productName.text = product.name
        productError.text = product.errorDescription
        productSKU.text = product.sku
        ProductColor.text = product.color ?? "No color"
    }
}
