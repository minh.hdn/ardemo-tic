//
//  PageCollectionViewCell.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import UIKit

class PageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var numberOfPage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        numberOfPage.textColor = AppColor.shadowColor
        self.layer.cornerRadius = 10
    }
}

