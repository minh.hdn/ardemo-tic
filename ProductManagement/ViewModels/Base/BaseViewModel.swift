//
//  BaseViewModel.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Foundation

protocol IBaseViewModel { }

protocol BaseViewModelDelegate: AnyObject {
    func showLoading()
    func hideLoading()
    func showErrorMessage(message: String)
}
