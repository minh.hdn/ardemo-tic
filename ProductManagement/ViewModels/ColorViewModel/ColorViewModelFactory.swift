//
//  ColorViewModelFactory.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Moya

class ColorViewModelFactory: ViewModelFactory {
    typealias Target = ColorService
    typealias M = ColorManager
    typealias V = ColorViewModel
    
    func makeProvider() -> MoyaProvider<ColorService> {
        return createMoyaProvider(targetType: ColorService.self)
    }
    
    func makeManager() -> ColorManager {
        return ColorManager(colorProvider: makeProvider())
    }
    
    func makeViewModel() -> ColorViewModel {
        return ColorViewModel(colorManager: makeManager())
    }
}
