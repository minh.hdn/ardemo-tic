//
//  ColorViewModel.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Foundation
//import RealmSwift

protocol ColorViewModelDelegate: BaseViewModelDelegate {
    
    func readedColors()
}

class ColorViewModel: IBaseViewModel {
    private var colors = [Color]()
    let colorManager: ColorManager
    var delegate: ColorViewModelDelegate?
    
    init(colorManager: ColorManager) {
        self.colorManager = colorManager
    }
    
    func readedColors() {
        self.delegate?.showLoading()
        colorManager.readColors { (colors, error) in
            self.delegate?.hideLoading()
            if let error = error {
                self.delegate?.showErrorMessage(message: error.localizedDescription)
            }
            self.colors = colors!
            self.delegate?.readedColors()
        }
    }
    
    func getAllColor() -> [Color] {
        return colors
    }
    
    func numberOfRowsInSection() -> Int {
        return colors.count
    }

    func getColor(at index: Int) -> Color {
        return colors[index]
    }
    
    func getColorById(at id: Int) -> Color? {
        return colors.first(where: {$0.id == id})
    }
}
