//
//  ProductViewModel.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Foundation
import RealmSwift

protocol ProductViewModelDelegate: BaseViewModelDelegate {
    
    func readedProducts()
}

class ProductViewModel: IBaseViewModel {
    private var products = [LocalProduct]()

    let productManager: ProductManager
    var delegate: ProductViewModelDelegate?
    let realm = try! Realm()
    
    init(productManager: ProductManager) {
        self.productManager = productManager
    }
    
    func readedProducts() {
        self.delegate?.showLoading()
        productManager.readProducts { (products, error) in
            self.delegate?.hideLoading()
            if let error = error {
                self.delegate?.showErrorMessage(message: error.localizedDescription)
            }
            if let products {
                if self.realm.objects(LocalProduct.self).isEmpty {
                    try! self.realm.write {
                        for i in products {
                            let product = LocalProduct()
                            product.id = i.id
                            product.name = i.name
                            product.errorDescription = i.errorDescription
                            product.image = i.image
                            product.sku = i.sku
                            product.color = i.color
                            product.width = 0.37
                            self.realm.add(product)
                            self.products.append(product)
                        }
                        print(">>> Add all products to local")
                    }
                }
                else {
                    for i in self.realm.objects(LocalProduct.self) {
                        self.products.append(i)
                    }
                    print(">>> Products has added!")
                }
                //self.products.append(product)
            }
            self.delegate?.readedProducts()
        }
    }
    
    func getProductView(product: LocalProduct,colors: [Color]) -> ProductView{
        let productView = ProductView()
        productView.id = product.id
        productView.name = product.name
        productView.errorDescription = product.errorDescription
        productView.sku = product.sku
        productView.image = product.image
        productView.color = colors.first(where: {$0.id == product.color})?.name
        return productView
    }
    
    func numberOfRowsInSection() -> Int {
        return products.count
    }
    
    func getProduct(at index: Int) -> LocalProduct {
        return products[index]
    }
    
    func getProductPage(at page: Int) -> [LocalProduct] {
        var pageList = [LocalProduct]()
        let start = page*10
        let end = start+10
        if(page*10+10 < products.count) {
            for i in stride(from: start, to: end, by: 1) {
                pageList.append(products[i])
            }
        }
        else {
            for i in stride(from: start, to: products.count, by: 1) {
                pageList.append(products[i])
            }
        }
        return pageList
    }

    func getProductById(at id: Int) -> LocalProduct? {
        return products.first(where: {$0.id == id})
    }
    
    func updateProduct(newProduct: LocalProduct) {
        if let index = products.firstIndex(where: {$0.id == newProduct.id}) {
            products[index] = newProduct
            print(">>> Update item have id = \(newProduct.id)")
        }
    }
    
    func updateProductLocal(listProduct: [LocalProduct]) {
        let productLocal = realm.objects(LocalProduct.self)
        for i in listProduct {
            if let product = productLocal.first(where: { $0.id == i.id }) {
                try! realm.write {
                    product.name = i.name
                    product.sku = i.sku
                    product.color = i.color
                    print(">>> Update local item have id =  \(product.id)")
                }
            }
        }
        products.removeAll()
        for i in self.realm.objects(LocalProduct.self) {
            self.products.append(i)
        }
    }
}
