//
//  ProductViewModelFactory.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Moya

class ProductViewModelFactory: ViewModelFactory {
    typealias Target = ProductService
    typealias M = ProductManager
    typealias V = ProductViewModel
    
    func makeProvider() -> MoyaProvider<ProductService> {
        return createMoyaProvider(targetType: ProductService.self)
    }
    
    func makeManager() -> ProductManager {
        return ProductManager(productProvider: makeProvider())
    }
    
    func makeViewModel() -> ProductViewModel {
        return ProductViewModel(productManager: makeManager())
    }
}
