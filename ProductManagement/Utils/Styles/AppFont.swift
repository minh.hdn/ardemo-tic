//
//  AppFont.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Foundation
import UIKit

enum FontType: String {
    case regular        = "-Regular"
    case medium         = "-Medium"
    case semiBold       = "-Semibold"
    case bold           = "-Bold"
}

enum FontName: String {
    case sfProText    = "SFProText"
    case sfProDisplay = "SFProDisplay"
    case sfPro = "SFPro"
}

enum FontScale: CGFloat {
    case iPhone     = 1.0
    case iPad       = 1.135
}

let fontScale: FontScale = UIDevice.current.userInterfaceIdiom == .phone ? .iPhone : .iPad

struct AppFont {
    public static func font(_ name: FontName, type: FontType, size: CGFloat) -> UIFont {
        let fontName = name.rawValue + type.rawValue
        let fontSize = size * fontScale.rawValue
        let font = UIFont(name: fontName, size: fontSize)
        if let font = font {
            return font
        } else {
            return UIFont.systemFont(ofSize: fontSize)
        }
    }
}
