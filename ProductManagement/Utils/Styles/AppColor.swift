//
//  AppColor.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import UIKit

struct AppColor {
    static let backgroundColor = UIColor.init(hexString: "#F3F5F9")
    static let textColor = UIColor.init(hexString: "#212B38")
    static let mainColor = UIColor.init(hexString: "#5468FF")
    static let shadowColor = UIColor.init(hexString: "#B9BDC3")
}
