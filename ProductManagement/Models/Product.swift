//
//  Product.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Foundation
import RealmSwift

struct Product: Codable {
    let id: Int
    let errorDescription: String
    let name: String
    let sku: String
    let image: String
    let color: Int?
    let width: Double?
}

class LocalProduct: Object {
    @Persisted var id: Int
    @Persisted var errorDescription: String
    @Persisted var name : String
    @Persisted var sku: String
    @Persisted var image: String
    @Persisted var color: Int?
    @Persisted var width: Double?
}

class ProductView: Object {
    @Persisted var id: Int
    @Persisted var errorDescription: String
    @Persisted var name : String
    @Persisted var sku: String
    @Persisted var image: String
    @Persisted var color: String?
}


