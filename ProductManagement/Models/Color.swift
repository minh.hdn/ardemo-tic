//
//  Color.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Foundation
import RealmSwift

struct Color: Codable {
    let id: Int
    let name: String
}

