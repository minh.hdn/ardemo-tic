//
//  NetworkUtil.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Foundation

enum APIEnvironment {
    case staging
    case dev
    case production
}

struct NetworkUtil {
    private static let environment: APIEnvironment = .dev

    static var environmentBaseURL: String {
        switch NetworkUtil.environment {
        case .production: return "https://hiring-test.stag.tekoapis.net"
        case .staging: return "https://hiring-test.stag.tekoapis.net"
        case .dev: return "https://hiring-test.stag.tekoapis.net"
        }
    }
}
