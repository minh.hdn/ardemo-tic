//
//  ColorService.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Moya

enum ColorService {
    case readColors
}

extension ColorService: TargetType {

    var baseURL: URL {
        guard let url = URL(string: NetworkUtil.environmentBaseURL) else { fatalError("baseURL could not be configured") }
        return url
    }

    var path: String {
        switch self {
        case .readColors:
            return "/api/colors"
        }
    }

    var method: Moya.Method {
        switch self {
        case .readColors:
            return .get
        }
    }

    var task: Task {
        switch self {
        case .readColors:
            return .requestPlain
        }
    }

    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
}

