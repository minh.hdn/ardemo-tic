//
//  ColorManager.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Moya

protocol ColorProtocol {
    func readColors(completion: @escaping ([Color]?, Error?) -> Void)

}

class ColorManager: BaseManager <ColorService, MoyaProvider<ColorService>>, ColorProtocol {
    
    init(colorProvider: MoyaProvider<ColorService>) {
        super.init(provider: colorProvider)
    }
    
    func readColors(completion: @escaping ([Color]?, Error?) -> Void) {
        mRequest(.readColors, callback: completion)
    }
    
}
