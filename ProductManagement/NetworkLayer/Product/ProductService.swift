//
//  ProductService.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Moya

enum ProductService {
    case readProducts
}

extension ProductService: TargetType {

    var baseURL: URL {
        guard let url = URL(string: NetworkUtil.environmentBaseURL) else { fatalError("baseURL could not be configured") }
        return url
    }

    var path: String {
        switch self {
        case .readProducts:
            return "/api/products"
        }
    }

    var method: Moya.Method {
        switch self {
        case .readProducts:
            return .get
        }
    }

    var task: Task {
        switch self {
        case .readProducts:
            return .requestPlain
        }
    }

    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
}
