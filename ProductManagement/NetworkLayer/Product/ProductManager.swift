//
//  ProductManager.swift
//  ProductManagement
//
//  Created by SMin on 31/12/2022.
//

import Moya

protocol ProductProtocol {
    func readProducts(completion: @escaping ([Product]?, Error?) -> Void)

}

class ProductManager: BaseManager <ProductService, MoyaProvider<ProductService>>, ProductProtocol {
    
    init(productProvider: MoyaProvider<ProductService>) {
        super.init(provider: productProvider)
    }
    
    func readProducts(completion: @escaping ([Product]?, Error?) -> Void) {
        mRequest(.readProducts, callback: completion)
    }
    
}
